import 'package:flutter/material.dart';
import 'package:flutter_shop_app/screens/details/components/cartCounter.dart';
import 'package:flutter_svg/flutter_svg.dart';

class CounterWithFavButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        CartCounter(),
        Container(
          padding: EdgeInsets.all(8),
          height: 32, 
          width: 32,
          decoration: BoxDecoration(
            color: Color(0xffff6464),
            shape: BoxShape.circle,
          ),
          child: SvgPicture.asset("assets/icons/heart.svg")
        )
      ],
    );
  }
}