import 'package:flutter/material.dart';
import 'package:flutter_shop_app/constants.dart';

class Description extends StatelessWidget {
  final String description;

  Description({
    Key key,
    this.description
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: kDefaultPadding),
      child: Text(
        description,
        style: TextStyle(height: 1.5),
      )
    );
  }
}