import 'package:flutter/material.dart';
import 'package:flutter_shop_app/constants.dart';
import 'package:flutter_shop_app/models/Product.dart';
import 'package:flutter_shop_app/screens/details/components/colorDot.dart';

class ColorAndSize extends StatelessWidget {

  final Product product;

  const ColorAndSize({
    Key key,
    this.product
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text("Color"),
              Row(
                children: <Widget>[
                  ColorDot(
                    color: Color(0xff356c95),
                    isSelected: true,
                  ),
                  ColorDot(color: Color(0xfff8c078)),
                  ColorDot(color: Color(0xffa29b9b))
                ],
              )
            ]
          ),
        ),
        Expanded(
          child: RichText(
            text: TextSpan(
              style: TextStyle(color: kTextColor),
              children: [
                TextSpan(text: "Size\n"),
                TextSpan(
                  text: "${product.size} cm",
                  style: Theme.of(context).textTheme.headline5.copyWith(fontWeight: FontWeight.bold)
                )
              ],
            )
          )
        )
      ]
    );
  }
}