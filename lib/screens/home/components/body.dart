import 'package:flutter/material.dart';
import 'package:flutter_shop_app/screens/details/details_screen.dart';
import 'package:flutter_shop_app/screens/home/components/categories.dart';
import 'package:flutter_shop_app/screens/home/components/itemCard.dart';
import 'package:flutter_shop_app/constants.dart';
import 'package:flutter_shop_app/data/products.dart';


class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: kDefaultPadding),
          child: Text(
            "Women",
            style: Theme.of(context)
              .textTheme.
              headline5
              .copyWith(fontWeight: FontWeight.bold)
          ),
        ),
        Categories(),
        Expanded(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: kDefaultPadding),
            child: GridView.builder(
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                childAspectRatio: 0.7,
                mainAxisSpacing: kDefaultPadding,
                crossAxisSpacing: kDefaultPadding
              ),
              itemCount: products.length,
              itemBuilder: (context, index) => ItemCard(
                product: products[index],
                press: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => DetailsScreen(product: products[index])
                  )
                )
              )
            )
          )
        )
      ],
    );
  }
}