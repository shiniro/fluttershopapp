import 'package:flutter/material.dart';

class Product {
  final String title, image, description;
  final int id, price, size;
  final Color color;

  Product({
    this.id,
    this.title,
    this.image,
    this.price,
    this.description,
    this.size,
    this.color
  });
}